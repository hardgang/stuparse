import { Parser, ParserRef, resolve } from "./parser";
import { Success, Failure, success, failure } from "./replies";

export function optional<R>(pr: ParserRef<R>, defaultValue: R|undefined = undefined) {
  return new Parser<R|undefined>("optional", state => {
    let p = resolve(pr);
    let reply = p.matcher(state);
    if (reply.success) {
      return reply;
    }
    return success(state, 0, defaultValue);
  });
}

// TODO: test
function not<R>(pr: ParserRef<R>) {
  return new Parser<R|undefined>("not", state => {
    let p = resolve(pr);
    let reply = p.matcher(state);
    if (reply.success) {
      let success = reply as Success<R>;
      // state.retreat(success.length);
      return failure(state, `Expected: not "${p.name}".`);
    }
    return success(state, 0, undefined);
  });
}

export function choice<R>(...rs: ParserRef<R>[]) {
  return new Parser<R>("choice", state => {
    let ps = rs.map(resolve);
    for (let i = 0; i < ps.length; i++) {
      let reply = ps[i].matcher(state);
      if (reply.success) return reply;
    }
    return failure(state, "No matches.");
  });
}

export function either<R1, R2>(pr1: ParserRef<R1>, pr2: ParserRef<R2>) {
  return new Parser<R1|R2>("either", state => {
    let p1 = resolve(pr1);
    let p2 = resolve(pr2);
    let reply1 = p1.matcher(state);
    if (reply1.success) return reply1;
    return p2.matcher(state);
  });
}

export function either3<R1, R2, R3>(pr1: ParserRef<R1>, pr2: ParserRef<R2>, pr3: ParserRef<R3>) {
  return new Parser<R1|R2|R3>("either3", state => {
    let p1 = resolve(pr1);
    let p2 = resolve(pr2);
    let p3 = resolve(pr3);
    let reply2 = either(p1, p2).matcher(state);
    if (reply2.success) return reply2;
    return p3.matcher(state);
  });
}

export function either4<R1, R2, R3, R4>(pr1: ParserRef<R1>, pr2: ParserRef<R2>, pr3: ParserRef<R3>,
    pr4: ParserRef<R4>) {
  return new Parser<R1|R2|R3|R4>("either4", state => {
    let p1 = resolve(pr1);
    let p2 = resolve(pr2);
    let p3 = resolve(pr3);
    let p4 = resolve(pr4);
    let reply3 = either3(p1, p2, p3).matcher(state);
    if (reply3.success) return reply3;
    return p4.matcher(state);
  });
}

export function either5<R1, R2, R3, R4, R5>(pr1: ParserRef<R1>, pr2: ParserRef<R2>, pr3: ParserRef<R3>,
    pr4: ParserRef<R4>, pr5: ParserRef<R5>) {
  return new Parser<R1|R2|R3|R4|R5>("either5", state => {
    let p1 = resolve(pr1);
    let p2 = resolve(pr2);
    let p3 = resolve(pr3);
    let p4 = resolve(pr4);
    let p5 = resolve(pr5);
    let reply4 = either4(p1, p2, p3, p4).matcher(state);
    if (reply4.success) return reply4;
    return p5.matcher(state);
  });
}