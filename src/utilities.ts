export function first<T>(vs: T[]) {
  return vs[0];
}

export function second<T>(vs: T[]) {
  return vs[1];
}

export function third<T>(vs: T[]) {
  return vs[2];
}

export function fourth<T>(vs: T[]) {
  return vs[3];
}

export function fifth<T>(vs: T[]) {
  return vs[4];
}

export function nth<T>(n: number) {
  return (vs: T[]) => vs[n];
}

export function negate<T extends Function>(fn: T) {
  return (...args: any[]) => !fn.apply(this, args);
}

export function isFunction(x: any) {
  return Object.prototype.toString.call(x) === "[object Function]";
}

export function isString(x: any) {
  return typeof x === "string" || x instanceof String;
}

export function isRegExp(x: any) {
  return Object.prototype.toString.call(x) === "[object RegExp]";
}

export function escapeRegExp(s: string) {
  return s.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&");
}

export function escapeRegExpSet(s: string) {
  return s.replace(/[|\\{}()[\]^$+*?.-]/g, "\\$&");
}

export function dump(x: any) {
  console.log(JSON.stringify(x, null, 2));
}

export function countBreaks(s: string) {
  return (s.match(/\n/g) || []).length;
}
