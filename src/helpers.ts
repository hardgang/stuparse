import { first, escapeRegExp, escapeRegExpSet } from "./utilities";
import { string, regex } from "./primitives";

// TODO: anyOf(string[])
// TODO: noneOf(string[])
// TODO: restOfLine

export const letter = regex(/[A-Za-z]/).map(first).named("letter");
export const letters = regex(/[A-Za-z]+/).map(first).named("letters");
export const lower = regex(/[a-z]/).map(first).named("lowercase character");
export const upper = regex(/[A-Z]/).map(first).named("uppercase character");
export const lowers = regex(/[a-z]+/).map(first).named("lowercase string");
export const uppers = regex(/[A-Z]+/).map(first).named("uppercase string");
export const digit = regex(/[0-9]/).map(first).named("digit");
export const digits = regex(/[0-9]+/).map(first).named("digits");
export const hex = regex(/[0-9a-fA-F]/).map(first).named("hex character");
export const hexes = regex(/[0-9a-fA-F]+/).map(first).named("hex string");
export const octal = regex(/[0-7]/).map(first).named("octal character");
export const octals = regex(/[0-7]+/).map(first).named("octal string");
export const spaces = regex(/[ \t\n\r]*/).map(first).named("whitespace");
export const spaces1 = regex(/[ \t\n\r]+/).map(first).named("required whitespace");
export const integer = regex(/[-+]?[0-9]+/).map(first).named("integer");
export const float = regex(/[-+]?[0-9]+(\.[0-9]+)?/).map(first).named("float");

export const tab = string("\t");
export const tick = string("'");
export const quote = string(`"`);

export function stringin(s: string) {
  return regex(new RegExp(escapeRegExp(s), "i")).map(first).named("string");
}

export function anyChar(s: string) {
  return regex(new RegExp(`[${escapeRegExpSet(s)}]`)).map(first);
}

export function anyChars(s: string) {
  return regex(new RegExp(`[${escapeRegExpSet(s)}]+`)).map(first);
}

export function anyCharExcept(s: string) {
  return regex(new RegExp(`[^${escapeRegExpSet(s)}]`)).map(first);
}

export function anyCharsExcept(s: string) {
  return regex(new RegExp(`[^${escapeRegExpSet(s)}]+`)).map(first);
}

