import { countBreaks } from "./utilities";
import { Reply } from "./replies";

/* Represents the position of the State at a point in time. */
export interface Location {
  position: number;
  line: number;
  column: number;
}

/** Tracks the position of the parser. */
export class State {
  private src: string;
  private p = 0;
  private ln = 1;
  private col = 1;
  log: Reply[] = [];

  constructor(source: string) {
    this.src = source;
  }

  get source() {
    return this.src;
  }

  /** Current position. */
  get position() {
    return this.p;
  }

  /** Current line number. */
  get line() {
    return this.ln;
  }

  /** Current column number. */
  get column() {
    return this.col;
  }

  /** Move the parser position forward. */
  advance(length: number) {
    let start = this.p;
    this.p += length;
    let span = this.src.substring(start, start + length);
    this.ln += countBreaks(span);
    let lastBreak = span.lastIndexOf("\n");
    this.col = (lastBreak === -1) ? this.col + length : length - lastBreak;
  }

  /** Move the parser position backward. */
  retreat(length: number) {
    let end = this.p;
    this.p -= length;
    let span = this.src.substring(end - length, end);
    this.ln -= countBreaks(span);
    this.col = span.lastIndexOf("\n") === -1
      ? this.col - length
      : Math.max(1, this.p - this.src.lastIndexOf("\n", this.p));
  }

  /** Returns a snapshot of the parser's position. */
  locate(): Location {
    return {
      position: this.position,
      line: this.line,
      column: this.column,
    };
  }
}
