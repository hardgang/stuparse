import { Parser, ParserRef, DeferParser, resolve, defer } from "./parser";
import { Success, Failure, success, failure } from "./replies";
import { succeed } from "./primitives";

// TODO: manySeparated
// TODO: manyIgnore

export function many<R>(pr: ParserRef<R>, min = 0, max = Infinity) {
  return new Parser<R[]>("many", state => {
    let p = resolve(pr);
    let successes: Success<R>[] = [];
    let reply = p.matcher(state);
    while (reply.success) {
      successes.push(reply as Success<R>);
      reply = p.matcher(state);
    }
    let length = successes.reduce((m, s) => m + s.length, 0);
    if (successes.length < min) {
      state.retreat(length);
      return failure(state, `Expecting at least ${min} ${p.name}(s).`);
    }
    else if (successes.length > max) {
      state.retreat(length);
      return failure(state, `Expecting at most ${max} ${p.name}(s).`);
    }
    return success(state, length, successes.map(s => s.value));
  });
}

export function many1<R>(p: ParserRef<R>, max = Infinity) {
  return many(p, 1, max);
}

export function manySeparated<R1, R2>(pr: ParserRef<R1>, sepr: ParserRef<R2>, min = 0, max = Infinity) {
  min = Math.max(0, min - 1);
  max = max - 1;
  return defer(() => {
    let p = resolve(pr);
    let sep = resolve(sepr);
    let tail = sep.ignoreThen(p).many(min, max);
    return p.then(tail).map(([head, tail]) => [head].concat(tail));
  });
}

export function reduce<R1, R2, R3>(pr: ParserRef<R1>, sepr: ParserRef<R2>,
    first: (v: R1) => R3 = (v: R1) => [v] as any as R3,
    next: (sum: R3, sep: R2, item: R1) => R3 = (sum: R3, sep: R2, item: R1) =>
      (sum as any as R1[]).push(item) as any as R3,
    min = 0, max = Infinity): Parser<R3> {
  min = Math.max(0, min - 1);
  max = max - 1;
  return defer(() => {
    let p = resolve(pr);
    let sep = resolve(sepr);
    let tail = sep.then(p).many(min, max);
    return p.then(tail).map(([head, tail]) => {
      return tail.reduce((sum, [sep, item]) => {
        return next(sum, sep, item);
      }, first(head));
    });
  });
}
