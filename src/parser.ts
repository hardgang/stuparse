import { first, second, negate, isFunction, isString, isRegExp,
  countBreaks } from "./utilities";
import { State, Location } from "./state";
import { Success, Failure, success, failure } from "./replies";
import { chain, chain3 } from "./chains";
import { many, manySeparated } from "./repeats";
import { either, optional } from "./choices";
import { string, regex } from "./primitives";

/** Function which attempts to consume an instance of R, updating state accordingly. */
export type Matcher<R> = (state: State) => Success<R> | Failure;

/** Callback which returns a parser. */
export type DeferParser<R> = () => Parser<R>;

/** Alias to ease automatic casting of deferred parsers and primitives. */
export type ParserRef<R> = Parser<R> | DeferParser<R> | R;

/** Converts a parser reference to a parser instance. */
export function resolve<R>(p: Parser<R> | DeferParser<R> | R): Parser<R> {
  if (isFunction(p)) {
    return (p as DeferParser<R>)();
  }
  else if (isString(p)) {
    return string(p as any as string) as any as Parser<R>;
  }
  else if (isRegExp(p)) {
    return regex(p as any as RegExp) as any as Parser<R>;
  }
  return p as Parser<R>;
}

/** Creates a deferred parser. */
export function defer<R>(p: DeferParser<R>) {
  return new Parser<R>("deferred", state => {
    return p().matcher(state);
  });
}

/** Parses an instance of R from text. */
export class Parser<R> {
  name: string;
  matcher: Matcher<R>;

  constructor(name: string, matcher: Matcher<R>) {
    this.name = name;
    this.matcher = matcher;
  }

  // TODO: not
  // TODO: reject

  exec(source: string) {
    let state = new State(source);
    return this.matcher(state);
  }

  run(source: string) {
    let state = new State(source);
    let reply = this.matcher(state);
    if (!reply.success) {
      state.log.forEach(r => {
        if (r.success) {
          let { location, length, value } = r as Success<any>;
          console.log(`success at ${location.position}-${location.position + length}: ${value.toString().replace("\n", " ")}`);
        }
        else {
          let { error, location } = r as Failure;
          console.log(`failure at ${location.line}: ${error}`);
        }
      });
      throw new Error((reply as Failure).error);
    }
    return (reply as Success<R>).value;
  }

  named(name: string) {
    this.name = name;
    return this;
  }

  map<R2>(fn: (value: R, location: Location) => R2) {
    return new Parser<R2>(this.name, state => {
      let location = state.locate();
      let reply = this.matcher(state);
      if (!reply.success) return reply as Failure;
      let { length, value } = reply as Success<R>;
      return success(state, length, fn(value, location));
    });
  }

  filter(fn: (value: R) => boolean) {
    return new Parser<R>(this.name, state => {
      let reply = this.matcher(state);
      if (!reply.success) return reply as Failure;
      let { length, value } = reply as Success<R>;
      if (!fn(value)) {
        state.retreat(length);
        return failure(state, `${this.name} did not pass filter.`);
      }
      return reply as Success<R>;
    });
  }

  reject(fn: (value: R) => boolean) {
    return this.filter(negate(fn));
  }

  then<R2>(p: ParserRef<R2>): Parser<[R, R2]> {
    return chain(this, p);
  }

  thenIgnore<R2>(p: ParserRef<R2>): Parser<R> {
    return chain(this, p).map(first) as Parser<R>;
  }

  ignoreThen<R2>(p: ParserRef<R2>): Parser<R2> {
    return chain(this, p).map(second) as Parser<R2>;
  }

  ignoreLeading<R2>(p: ParserRef<R2>): Parser<R> {
    return chain(p, this).map(second) as Parser<R>;
  }

  between<R2>(p: ParserRef<R2>): Parser<R> {
    return chain3(p, this, p).map(second) as Parser<R>;
  }

  enclosedBy<R2, R3>(p2: ParserRef<R2>, p3: ParserRef<R3>): Parser<R> {
    return chain3(p2, this, p3).map(second) as Parser<R>;
  }

  or<R2>(p: ParserRef<R2>): Parser<R|R2> {
    return either(this, p);
  }

  many(min = 0, max = Infinity): Parser<R[]> {
    return many(this, min, max);
  }

  many1(max = Infinity): Parser<R[]> {
    return many(this, 1, max);
  }

  manySeparated<R2>(sep: ParserRef<R2>, min = 0, max = Infinity): Parser<R[]> {
    return manySeparated(this, sep, min, max);
  }

  anyOf<V>(values: V[]) {
    return this.filter(v => values.indexOf(v as any) !== -1);
  }

  noneOf<V>(values: V[]) {
    return this.filter(v => values.indexOf(v as any) === -1);
  }

  optional(): Parser<R|undefined> {
    return optional(this);
  }
}
