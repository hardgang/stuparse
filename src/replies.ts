import { State, Location } from "./state";

/** The result of executing a parser. */
export interface Reply {
  success: boolean;
}

/** Represents a successful execution of the parser. */
export interface Success<R> extends Reply {
  value: R;
  location: Location;
  length: number;
};

/** Represents a failed execution of the parser. */
export interface Failure extends Reply {
  error: string;
  location: Location;
}

/** Creates a Success. */
export function success<R>(state: State, length: number, value: R): Success<R> {
  let success = {
    success: true,
    value,
    length,
    location: state.locate(),
  };
  state.log.push(success);
  return success;
}

/** Creates a Failure. */
export function failure(state: State, error: string): Failure {
  let failure = {
    success: false,
    error,
    location: state.locate(),
  };
  state.log.push(failure);
  return failure;
}
