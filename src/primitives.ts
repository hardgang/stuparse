import { Parser } from "./parser";
import { success, failure } from "./replies";

/** Parser which matches the specified string. */
export function string(s: string) {
  return new Parser<string>("string", state => {
    let span = state.source.substring(state.position, state.position + s.length);
    if (span === s) {
      state.advance(s.length);
      return success(state, s.length, span);
    }
    return failure(state, `Expected: '${s}' but got '${span}'.`);
  });
}

/** Parser which matches the specified regex. */
export function regex(r: RegExp) {
  let i = r.ignoreCase ? "i" : "";
  let m = r.multiline ? "m" : "";
  let source = r.source[0] === "^" ? r.source : ("^" + r.source);
  let regex = new RegExp(source, i + m);
  let name = `/${r.source}/${i}${m}`;
  return new Parser<RegExpExecArray>(name, state => {
    let tail = state.source.substring(state.position);
    let match = regex.exec(tail);
    if (match !== null) {
      state.advance(match[0].length);
      return success(state, match[0].length, match);
    }
    return failure(state, `Expected: /${name}.`);
  });
}

export function succeed<R>(value: R) {
  return new Parser<R>("succeed", state => success(state, 0, value));
}

export function fail<R>(error = "failed") {
  return new Parser<R>("fail", state => failure(state, error));
}

export function end() {
  return new Parser<undefined>("end", state => state.position === state.source.length
    ? success(state, 0, undefined)
    : failure(state, "Expected: end"));
}
