import { first, second } from "./utilities";
import { Parser, ParserRef, resolve } from "./parser";
import { Success, Failure, success, failure } from "./replies";

// TODO: between
// TODO: followedBy
// TODO: preceededBy

export function check<R>(pr: ParserRef<R>) {
  return new Parser<R>("check", state => {
    let p = resolve(pr);
    let reply = p.matcher(state);
    if (reply.success) {
      let success = reply as Success<R>;
      state.retreat(success.length);
    }
    return reply;
  });
}

export function chain<R1, R2>(pr1: ParserRef<R1>, pr2: ParserRef<R2>) {
  return new Parser<[R1, R2]>("chain", state => {
    let p1 = resolve(pr1);
    let p2 = resolve(pr2);
    let reply1 = p1.matcher(state);
    if (!reply1.success) return reply1 as Failure;
    let success1 = reply1 as Success<R1>;
    let reply2 = p2.matcher(state);
    if (!reply2.success) {
      state.retreat(success1.length);
      return reply2 as Failure;
    }
    let success2 = reply2 as Success<R2>;
    return success(state, success1.length + success2.length,
      [success1.value, success2.value] as [R1, R2]);
  });
}

export function chain3<R1, R2, R3>(pr1: ParserRef<R1>, pr2: ParserRef<R2>, pr3: ParserRef<R3>) {
  return new Parser<[R1, R2, R3]>("chain3", state => {
    let p1 = resolve(pr1);
    let p2 = resolve(pr2);
    let p3 = resolve(pr3);
    let reply2 = chain(p1, p2).matcher(state);
    if (!reply2.success) return reply2 as Failure;
    let success2 = reply2 as Success<[R1, R2]>;
    let reply3 = p3.matcher(state);
    if (!reply3.success) {
      state.retreat(success2.length);
      return reply3 as Failure;
    }
    let success3 = reply3 as Success<R3>;
    return success(state, success2.length + success3.length,
      [...success2.value, success3.value] as [R1, R2, R3]);
  });
}

export function chain4<R1, R2, R3, R4>(pr1: ParserRef<R1>, pr2: ParserRef<R2>, pr3: ParserRef<R3>,
    pr4: ParserRef<R4>) {
  return new Parser<[R1, R2, R3, R4]>("chain4", state => {
    let p1 = resolve(pr1);
    let p2 = resolve(pr2);
    let p3 = resolve(pr3);
    let p4 = resolve(pr4);
    let reply3 = chain3(p1, p2, p3).matcher(state);
    if (!reply3.success) return reply3 as Failure;
    let success3 = reply3 as Success<[R1, R2, R3]>;
    let reply4 = p4.matcher(state);
    if (!reply4.success) {
      state.retreat(success3.length);
      return reply4 as Failure;
    }
    let success4 = reply4 as Success<R4>;
    return success(state, success3.length + success4.length,
      [...success3.value, success4.value] as [R1, R2, R3, R4]);
  });
}

export function chain5<R1, R2, R3, R4, R5>(pr1: ParserRef<R1>, pr2: ParserRef<R2>, pr3: ParserRef<R3>,
    pr4: ParserRef<R4>, pr5: ParserRef<R5>) {
  return new Parser<[R1, R2, R3, R4, R5]>("chain4", state => {
    let p1 = resolve(pr1);
    let p2 = resolve(pr2);
    let p3 = resolve(pr3);
    let p4 = resolve(pr4);
    let p5 = resolve(pr5);
    let reply4 = chain4(p1, p2, p3, p4).matcher(state);
    if (!reply4.success) return reply4 as Failure;
    let success4 = reply4 as Success<[R1, R2, R3, R4]>;
    let reply5 = p5.matcher(state);
    if (!reply5.success) {
      state.retreat(success4.length);
      return reply5 as Failure;
    }
    let success5 = reply5 as Success<R5>;
    return success(state, success4.length + success5.length,
      [...success4.value, success5.value] as [R1, R2, R3, R4, R5]);
  });
}
