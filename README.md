# StuParse

> Toy parser combinator library written in TypeScript. I wrote this to test out some ideas, and ended up using packrattle because it's fast and the error handling is great.

- No dependencies
- Strongly typed
- Supports recursive parsers
- Extremely simple
- I am an amateur with parsing algorithms, I just like writing parsers
- Based on [packrattle](https://github.com/robey/packrattle) and inspiration from [FParsec](http://www.quanttec.com/fparsec/).
